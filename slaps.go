package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

var Fortunes = make(map[string][]string)

func SendSlaps(from int, text string) {
	if strings.TrimSpace(text) == "" {
		return
	}
	spl := strings.Split(text, " ")
	which, text := spl[0], strings.Join(spl[1:], " ")
	length := len(Fortunes[which])
	if length == 0 {
		return
	}
	rand.Seed(time.Now().Unix())
	line := Fortunes[which][rand.Intn(length)]
	message := ""
	if text == "" {
		message = fmt.Sprintf("/me golpea %s", line)
	} else {
		message = fmt.Sprintf("/me golpea a %s %s", text, line)
	}
	SendMessage(message, from, "")
}

func FortuneImport() error {
	var files = [5]string{
		"lobo",
		"archco",
		"fefes",
		"rengo",
		"ast",
	}
	for i := 0; i < len(files); i++ {
		lines, err := file2slaps(fmt.Sprintf("fortune/%s", files[i]))
		if err != nil {
			return err
		}
		Fortunes[files[i]] = lines
	}
	return nil
}

func file2slaps(inp string) ([]string, error) {
	file, err := os.Open(inp)
	if err != nil {
		return nil, err
	}
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(content), "\n")
	length := len(lines)
	if length%2 == 1 {
		length++
	}
	length = length >> 1
	out := make([]string, length, len(lines))
	j := 0
	for i := 0; i < len(lines); i++ {
		line := strings.TrimSpace(lines[i])
		if line != "" && line != "%" {
			out[j] = line
			j++
		}
	}
	return out, nil
}
